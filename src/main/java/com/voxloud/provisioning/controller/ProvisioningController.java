package com.voxloud.provisioning.controller;

import java.util.HashMap;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.service.ProvisioningServiceImpl;

//GET /api/v1/provisioning/aa-bb-cc-11-22-33

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

	//Inject the Service Class
	@Autowired
	ProvisioningServiceImpl provisioningServiceImpl;
	
    //This is the configuration method that is called
	//It is a GET method
	//The path variable macAddressOfDevice holds the value passed
	@GetMapping(value="/provisioning/{macAddressOfDevice}")
	public ResponseEntity<?> getConfigurationFile(@PathVariable("macAddressOfDevice") String macAddressOfDevice) {
		
		//Variable to hold the responseMessage to the Client.
		//If successful, this variable will return the config values
		String responseMessage  = "";
		String pathOfConfigFile = "";
		JSONObject responseMessageJSON = null;
		ResponseEntity<?> responseEntity = null;
		
		//Check if a value was passed for the MAC ADDRESS 
		//i.e. Check that the MAC Address value is not null
		if((macAddressOfDevice == null ? "" : macAddressOfDevice).equals("")) {
			//The MAC Address is null, return a response to the client
			return new ResponseEntity<>(
			          "MAC Address of device is required for this operation", 
			          HttpStatus.BAD_REQUEST);
		}
		
		//Check for the MAC Address in the inventory		
		//Object to hold the Device
		Optional<Device> deviceObject = provisioningServiceImpl.findDeviceByMacAddress(macAddressOfDevice);
		
		if(deviceObject.isPresent()) {
			//Device is found, so get configuration file
			pathOfConfigFile = provisioningServiceImpl.getProvisioningFile(macAddressOfDevice);
			
			//Check if the path of the file that has been returned is either
			// a json or a properties file
			if((pathOfConfigFile == null ? "" : pathOfConfigFile).contains(".properties")) {
				//This is the config file of a desk device
				//so return plain String by calling the utility function to get the
				//content of the ".properties" file
				responseMessage = provisioningServiceImpl.getFileContent(pathOfConfigFile);
				responseEntity = new ResponseEntity<>(
						responseMessage, 
				          HttpStatus.OK);
			}else 
				if((pathOfConfigFile == null ? "" : pathOfConfigFile).contains(".json")) {
					responseMessageJSON = provisioningServiceImpl.getJSONContent(pathOfConfigFile);
					responseEntity = new ResponseEntity<>(
							responseMessageJSON, 
					          HttpStatus.OK);
				}
		}else {
			
			responseMessage = "MAC Address supplied is not in Inventory";
			return new ResponseEntity<>(
					responseMessage, 
			          HttpStatus.NOT_FOUND);
		}
		
		return responseEntity;
	}
}