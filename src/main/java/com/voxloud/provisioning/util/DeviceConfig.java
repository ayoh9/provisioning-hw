package com.voxloud.provisioning.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeviceConfig {

	private String username;
	private String password;
	private String domain;
	private String port;
	private String codecs;
	
}
