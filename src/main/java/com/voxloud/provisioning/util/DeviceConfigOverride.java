package com.voxloud.provisioning.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeviceConfigOverride extends  DeviceConfig{
	private String timeout;
}
