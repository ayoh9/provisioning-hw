package com.voxloud.provisioning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<com.voxloud.provisioning.entity.Device, String> {
	
	//Custom JPQL Query to return String value of model
	@Query("SELECT d.model FROM Device d where d.id = :mac_address")
	String findModelById(@Param("mac_address") String  macAddress);
}
