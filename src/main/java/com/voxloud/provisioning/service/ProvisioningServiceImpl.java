package com.voxloud.provisioning.service;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import com.voxloud.provisioning.util.DataConverter;
import com.voxloud.provisioning.util.DeviceConfig;
import com.voxloud.provisioning.util.DeviceConfigOverride;
import com.voxloud.provisioning.util.DeviceOverride;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

	// Inject the DAO class using Dependency Injection
	@Autowired
	DeviceRepository deviceRepository;
	
	//Inject the Data Converter 
	@Autowired
	DataConverter dataConverter;

	// Fetch the domain value from Application.properties
	@Value("${provisioning.domain}")
	String domain;

	// Fetch the port value from Application.properties
	@Value("${provisioning.port}")
	String port;

	// Fetch the codecs value from Application.properties
	@Value("${provisioning.codecs}")
	String codecs;

	// Make use of a class variable for the device.
	// The device will be loaded when validating the mac address
	public Optional<Device> deviceFromDB;

	public Optional<Device> findDeviceByMacAddress(String macAddress) {

		// Find the Device from the DB by using the MAC Address
		// The MAC Address is also the object id as it is annotated
		// with "@Id" in the Entity Class
		deviceFromDB = deviceRepository.findById(macAddress);

		// Returns true if a value is found, false otherwise
		return deviceFromDB;
	}

	public String getProvisioningFile(String macAddress) {
		String configDataPath = "";

		// Check if the device is present
		// If present, proceed to generate the config file
		if (deviceFromDB.isPresent()) {
			// Get the Device
			Device device = deviceFromDB.get();

			// Check if the device has an override fragment
			if ((device.getOverrideFragment() == null ? "" : device.getOverrideFragment()).equals("")) {
				// The device has no Override Fragment
				DeviceConfig deviceConfig = new DeviceConfigOverride();

				deviceConfig.setUsername(device.getUsername());
				deviceConfig.setPassword(device.getPassword());
				deviceConfig.setDomain(domain);
				deviceConfig.setPort(port);
				deviceConfig.setCodecs(codecs);

				// Check the Device Type. This will determine the config type
				String deviceModel = deviceRepository.findModelById(macAddress);

				if (deviceModel.equals("DESK")) {

					configDataPath = buildAndSavePropertiesFile((DeviceConfigOverride) deviceConfig, macAddress);
				} else if (deviceModel.equals("CONFERENCE")) {

					configDataPath = buildAndSaveJSONFile((DeviceConfigOverride) deviceConfig, macAddress);
				} else {

				}
				
			} else {
				// The device has Override Fragment
				// Check the Device Type. This will determine the config type
				String deviceModel = deviceRepository.findModelById(macAddress);
				DeviceConfigOverride deviceConfigOverride = null;
				
				deviceConfigOverride = new DeviceConfigOverride();
				
				//Set the following properties from the database
				deviceConfigOverride.setUsername(device.getUsername());
				deviceConfigOverride.setPassword(device.getPassword());
				
				if (deviceModel.equals("DESK")) {
					// The device has override Fragment
					String[] deskOverride = device.getOverrideFragment().split("\\R");
					
					
					String domainString = deskOverride[0]; 
					String portString = deskOverride[1]; 
					String timeoutString = deskOverride[2];
					
					deviceConfigOverride.setDomain(domainString.substring(domainString.indexOf('=') + 1));
					deviceConfigOverride.setPort(portString.substring(portString.indexOf('=') + 1));
					deviceConfigOverride.setCodecs(codecs);
					deviceConfigOverride.setTimeout(timeoutString.substring(timeoutString.indexOf('=') + 1));
					
					configDataPath = buildAndSavePropertiesFile(deviceConfigOverride, macAddress);
				} else if (deviceModel.equals("CONFERENCE")) {
					// The device has override Fragment
					
					DeviceOverride deviceOverride = dataConverter.convertOverrideString(device.getOverrideFragment());
					
					deviceConfigOverride.setDomain(deviceOverride.getDomain() == null ? "" 
							: deviceOverride.getDomain());
					deviceConfigOverride.setPort(deviceOverride.getPort() == null ? "" 
							: deviceOverride.getPort());
					deviceConfigOverride.setCodecs(codecs);
					deviceConfigOverride.setTimeout(deviceOverride.getTimeout() == null ? "" 
							: deviceOverride.getTimeout());
					
					configDataPath = buildAndSaveJSONFile(deviceConfigOverride, macAddress);
				} else {

				}
				
			}
		}
		return configDataPath;
	}

	String buildAndSavePropertiesFile(DeviceConfigOverride deviceConfig, String macAddress) {

		// Instantiating the properties file
		Properties props = new Properties();
		// Instantiating the FileInputStream for output file
		OutputStream output = null;
		String filePath = "";
		// Variable to hold the file content
		String content = "";
		try {
			filePath = "ConfigFiles/" + macAddress + ".properties";
			output = new FileOutputStream(filePath);
			// Populating the properties file
			props.put("username", deviceConfig.getUsername());
			props.put("password", deviceConfig.getPassword());
			props.put("domain", deviceConfig.getDomain());
			props.put("port", deviceConfig.getPort());
			props.put("codecs", deviceConfig.getCodecs());
			if(!(deviceConfig.getTimeout() == null ? "" : deviceConfig.getTimeout()).equals("")) {
				props.put("timeout", deviceConfig.getTimeout());
			}
			// Storing the properties file
			// This may be necessary or important for support issues
			props.store(output, "This is the config file for device with MAC ADDRESS: " + macAddress);

			// Get the content of the file
			// content = getFileContent(filePath);
			content = filePath;
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return content;
	}

	private String buildAndSaveJSONFile(DeviceConfigOverride deviceConfig, String macAddress) {

		String filePath = "";

		// Creating a JSONObject object
		JSONObject jsonObject = new JSONObject();
		// Inserting key-value pairs into the json object
		jsonObject.put("username", deviceConfig.getUsername());
		jsonObject.put("password", deviceConfig.getPassword());
		jsonObject.put("domain", deviceConfig.getDomain());
		jsonObject.put("port", deviceConfig.getPort());
		jsonObject.put("codecs", deviceConfig.getCodecs());
		if(!(deviceConfig.getTimeout() == null ? "" : deviceConfig.getTimeout()).equals("")) {
			jsonObject.put("timeout", deviceConfig.getTimeout());
		}
		try {
			filePath = "ConfigFiles/" + macAddress + ".json";
			FileWriter file = new FileWriter(filePath);
			file.write(jsonObject.toString());
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filePath;
	}

	// This method is to get the content of the .properties file
	public String getFileContent(String path) {
		String content = "";

		try (Stream<String> lines = Files.lines(Paths.get(path))) {
			content = lines.collect(Collectors.joining(System.lineSeparator()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

	// This method is to get the content of the .properties file
	public JSONObject getJSONContent(String path) {
		JSONParser parser = null;
		JSONObject jsonObject = null;
		try {
			parser = new JSONParser();
			Object obj = parser.parse(new FileReader(path));
			jsonObject = (JSONObject) obj;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

}
