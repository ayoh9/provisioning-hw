package com.voxloud.provisioning.service;

import java.util.Map;

public interface ProvisioningService {

    String getProvisioningFile(String macAddress);
}
