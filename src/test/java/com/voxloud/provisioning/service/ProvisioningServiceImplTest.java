package com.voxloud.provisioning.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.entity.Device.DeviceModel;
import com.voxloud.provisioning.repository.DeviceRepository;
import com.voxloud.provisioning.util.DeviceConfig;
import com.voxloud.provisioning.util.DeviceConfigOverride;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProvisioningServiceImplTest {

	@MockBean
	DeviceRepository deviceRepository;

	@Autowired
	ProvisioningServiceImpl provisioningServiceImpl;
	
	@Test
	@DisplayName("Test Should Pass When The Object With The Specified Mac Address is returned")
	void testFindDeviceByMacAddress() {
		String macAddress = "aa-bb-cc-dd-ee-ff";
		
		Device device = new Device();
		device.setMacAddress(macAddress);
		device.setModel(DeviceModel.valueOf("DESK"));
		device.setOverrideFragment(null);
		device.setUsername("john");
		device.setPassword("doe");
		
		Optional<Device> deviceOptional = Optional.ofNullable(device);
		when(deviceRepository.findById(macAddress)).thenReturn(deviceOptional);
		assertTrue(deviceOptional.isPresent());
	}

	@Test
	@DisplayName("Test Should Pass When The Config Path Returned Contains The MAC Address")
	void testGetProvisioningFile() {
		// 
		String macAddress = "aa-bb-cc-dd-ee-ff";
		DeviceConfig deviceConfig = new DeviceConfigOverride();

		deviceConfig.setUsername("john");
		deviceConfig.setPassword("doe");
		deviceConfig.setDomain("domain");
		deviceConfig.setPort("5060");
		deviceConfig.setCodecs("G711,G729,OPUS");
		
		assertTrue(provisioningServiceImpl.buildAndSavePropertiesFile((DeviceConfigOverride) deviceConfig, macAddress)
				.contains(macAddress+".properties"));
	}

	

}
